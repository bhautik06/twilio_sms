const express = require('express')
const app = express()
const morgan = require('morgan')
const port = 3000
const indexRoute = require('./routes/index')


app.use(morgan('dev'))
app.use(express.json())
app.use(express.urlencoded({extended:false}))
app.use('/api', indexRoute)

app.get('/', (req, res) => res.send('Hello World'))

app.listen(port, () => {
    console.log(`server running on port ${port}`)
})